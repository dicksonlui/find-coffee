This small javascript Google Maps hack finds the best route to work with coffee and donuts along the way.

Replace the location and search queries for awesome functionality!

-Forgot that it's your girlfriend's birthday? Substitute coffee and donuts for flowers and chocolates, then enter her address!

-Going to a potluck at your parent's place and don't have anything good? Find a Chinese restaurant to get some great food and a container store so you can trick them into thinking you cooked it!